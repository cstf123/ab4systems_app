package com.example.ideea.ab4systems_app.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ideea.ab4systems_app.Activities.Main2Activity;
import com.example.ideea.ab4systems_app.Models.UserModel;
import com.example.ideea.ab4systems_app.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ideea on 06/05/2018.
 */

public class UserAdapter
        extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<UserModel> listOfUsers;
    private Context context;

    public UserAdapter(List<UserModel> listOfUsers, Context context1)
    {
        this.listOfUsers = listOfUsers;
        this.context = context1;
    }

    public List<UserModel> getListOfUsers() {
        return listOfUsers;
    }

    public void setListOfUsers(List<UserModel> listOfUsers) {
        this.listOfUsers = listOfUsers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText("Name: " + "\n" + listOfUsers.get(position).getName());
        Picasso.get().load(listOfUsers.get(position).getImageURL()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return listOfUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public ImageView imageView;

        public ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.nameA1);
            imageView = view.findViewById(R.id.imageA1);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int position = getAdapterPosition();
                    Intent intent = new Intent(context, Main2Activity.class);
                    intent.putExtra("KEY_NAME", listOfUsers.get(position).getName());
                    intent.putExtra("KEY_IMAGE", listOfUsers.get(position).getImageURL());
                    intent.putExtra("KEY_LOCATION", listOfUsers.get(position).getLocation());
                    intent.putExtra("KEY_BADGE_BRONZE", listOfUsers.get(position).getBadges().get("Bronze"));
                    intent.putExtra("KEY_BADGE_SILVER", listOfUsers.get(position).getBadges().get("Silver"));
                    intent.putExtra("KEY_BADGE_GOLD", listOfUsers.get(position).getBadges().get("Gold"));
                    context.startActivity(intent);
                }
            });
        }
    }
}
