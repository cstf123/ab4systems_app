package com.example.ideea.ab4systems_app.Activities;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ideea.ab4systems_app.R;
import com.squareup.picasso.Picasso;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        loadData();
    }

    //scoatem data transmisa de MainActivity + modificarea textului din textView-uri cu aceasta
    private void loadData() {
        ImageView imageView = findViewById(R.id.imageA2);
        TextView nameTextView = findViewById(R.id.nameA2);
        TextView locationTextView = findViewById(R.id.locationA2);
        TextView badgesTextView = findViewById(R.id.badgesA2);

        String name = "";
        String imageURL = "";
        String location = "";
        String bronze = "";
        String silver = "";
        String gold = "";

        Intent intent = getIntent();
        if(intent != null){
            name = intent.getStringExtra("KEY_NAME");
            imageURL = intent.getStringExtra("KEY_IMAGE");
            location = intent.getStringExtra("KEY_LOCATION");
            bronze = intent.getStringExtra("KEY_BADGE_BRONZE");
            silver = intent.getStringExtra("KEY_BADGE_SILVER");
            gold = intent.getStringExtra("KEY_BADGE_GOLD");
        }

        if(name != "") {
            nameTextView.setText("Name: " + name);
            Picasso.get().load(imageURL).into(imageView);
            locationTextView.setText("Location: " + location);
            badgesTextView.setText("Badges: " + "\n"
                                    + "Gold: " + gold + "\n"
                                    + "Silver: " + silver + "\n"
                                    + "Bronze" + bronze);
        }
    }
}
