package com.example.ideea.ab4systems_app.Models;

import java.util.HashMap;

/**
 * Created by Ideea on 06/05/2018.
 */

//model pentru user stackOverflow
/*{"items":[{
    "badge_counts":{
        "bronze":8205,
        "silver":7588,
        "gold":629
    },
    "location":"Reading, United Kingdom",
    "profile_image":"https://www.gravatar.com/avatar/6d8ebb117e8d83d74ea95fbdd0f87e13?s=128&d=identicon&r=PG",
    "display_name":"Jon Skeet"
 },
 ...*/
public class UserModel {
    private String name;
    private String imageURL;
    private String location;
    private HashMap<String, String> badges;

    public UserModel(String name, String imageURL, String location, HashMap<String, String> badges) {
        this.name = name;
        this.imageURL = imageURL;
        this.location = location;
        this.badges = badges;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public HashMap<String, String> getBadges() {
        return badges;
    }

    public void setBadges(HashMap<String, String> badges) {
        this.badges = badges;
    }
}
