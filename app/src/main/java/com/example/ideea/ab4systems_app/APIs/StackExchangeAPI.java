package com.example.ideea.ab4systems_app.APIs;

/**
 * Created by Ideea on 06/05/2018.
 */

public class StackExchangeAPI {
    private static final String URL = "https://api.stackexchange.com/2.2/users?pagesize=10&order=desc&sort=reputation&site=stackoverflow&filter=!40DEKwKLVP9gfqePP";

    public StackExchangeAPI() {
    }

    public static String getURL() {
        return URL;
    }
}
