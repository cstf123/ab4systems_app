package com.example.ideea.ab4systems_app.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ideea.ab4systems_app.APIs.StackExchangeAPI;
import com.example.ideea.ab4systems_app.R;
import com.example.ideea.ab4systems_app.Adapters.UserAdapter;
import com.example.ideea.ab4systems_app.Models.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    List<UserModel> listOfUsers;
    StackExchangeAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        api = new StackExchangeAPI();
        listOfUsers = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadRecyclerView();
    }

    //incarcam RecyclerView-ul cu top10-ul userilor de pe stackOverflow
    private void loadRecyclerView() {
        //Fetching Json
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                api.getURL(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("items");
                            for(int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject object = jsonArray.getJSONObject(i);

                                JSONObject badges = object.getJSONObject("badge_counts");
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("Bronze", badges.getString("bronze"));
                                hashMap.put("Silver", badges.getString("silver"));
                                hashMap.put("Gold", badges.getString("gold"));

                                UserModel user = new UserModel(object.getString("display_name"),
                                                                    object.getString("profile_image"),
                                                                    object.getString("location"),
                                                                    hashMap);
                                listOfUsers.add(user);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //incarcam datele in recyclerView
                        adapter = new UserAdapter(listOfUsers, getApplicationContext());
                        recyclerView.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
